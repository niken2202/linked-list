/*
 * LinkedList.cpp
 *
 *  Created on: Dec 4, 2017
 *      Author: Boss
 */

#include "LinkedList.h"

LinkedList::LinkedList() {
	head_=tail_=NULL;
	size_ = 0;
}

void LinkedList::addLast(int x) {
	Node* p = new Node(x);
	if (isEmpty()) {
		head_ = tail_ = p;
	} else {
		tail_->next(p);
		tail_ = p;
	}
	++size_;
}
void LinkedList::addFirst(int x) {
	Node* p = new Node(x);
	if (isEmpty()) {
		head_ = tail_ = p;
	} else {
		p->next(head_);
		head_ = p;
	}
	++size_;
}
void LinkedList::print() {
	Node* p = head_;
	while (p != NULL) {
		std::cout << p->data() << ' ';
		p = p->next();
	}
	std::cout << "->>size: " << getSize() << '\n';
}
void LinkedList::deleteLast() {
	Node* p = head_;

	while (p != NULL) {

		if (size_ == 1) {
			head_ = tail_ = NULL;
			--size_;
		} else {
			if (p->next() == tail_) {
				tail_ = p;
				tail_->next(NULL);
				--size_;
				return;
			}

		}

		p = p->next();
	}

}
void LinkedList::deleteFirst() {
	head_ = head_->next();
	--size_;
}
void LinkedList::addIndex(int x, int index) {
	Node*p = head_;
	Node*q = new Node(x);

	if (index > size_ || index < 0) {

		return;
	}
	if (index == size_) {

		addLast(x);
		return;
	}
	if (index == 0) {

		addFirst(x);
		return;
	}

	for (int i = 0; i < index - 1; ++i) {

		if (i == index - 1) {
			q->next(p->next()->next());
			p->next(q);
			++size_;
		}
		p = p->next();
	}

}
void LinkedList::removeIndex(int index) {
	Node* p = head_;
	if (index > size_ || index < 0) {

		return;
	}
	if (index == size_ - 1) {

		deleteLast();
		return;

	}
	if (index == 0) {

		deleteFirst();
		return;

	}
	for (int i = 0; i < index; ++i) {
		if (i == index - 1) {
			p->next(p->next()->next()) ;
			--size_;

			return;
		}
		p = p->next();

	}

}
bool LinkedList::isEmpty() {
	return size_ == 0;
}
int LinkedList::getSize(){
	return size_;
}
LinkedList::~LinkedList() {
	// TODO Auto-generated destructor stub
}

