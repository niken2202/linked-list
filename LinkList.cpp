//============================================================================
// Name        : LinkList.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "LinkedList.h"

int main() {
	LinkedList l;
	l.addFirst(1);
	l.addFirst(31);
	l.addFirst(5);
	l.addFirst(11);
	l.print();
//	l.addIndex(5, 0);
	l.addFirst(5);
	l.addIndex(5, 5);
	l.addIndex(6, 0);
	l.print();
	l.removeIndex(4);
	l.print();
	return 0;
}
