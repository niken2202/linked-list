/*
 * LinkedList.h
 *
 *  Created on: Dec 4, 2017
 *      Author: Boss
 */

#ifndef LINKEDLIST_H_
#define LINKEDLIST_H_
#include "Node.h"
#include <iostream>

class LinkedList {
private:
	int size_;
	Node* head_ = NULL;
	Node* tail_ = NULL;
public:
	LinkedList();
	virtual ~LinkedList();
	Node* head();
	Node* tail();
	bool isEmpty();
	void addLast(int x);
	void addFirst(int x);
	void deleteLast();
	void deleteFirst();
	void addIndex(int x, int index);
	void removeIndex(int index);
	void print();
	int getSize();

};

#endif /* LINKEDLIST_H_ */
