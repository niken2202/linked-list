/*
 * Node.cpp
 *
 *  Created on: Dec 4, 2017
 *      Author: Boss
 */

#include "Node.h"
#include <iostream>

Node::Node(int x) {

	this->data_ = x;
	this->next_ = NULL;
}

void Node::next(Node* p) {
	next_ = p;
}

Node* Node::next() {
	return next_;
}

int Node::data(){
	return data_;
}

Node::~Node() {
	// TODO Auto-generated destructor stub
}

