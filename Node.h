/*
 * Node.h
 *
 *  Created on: Dec 4, 2017
 *      Author: Boss
 */

#ifndef NODE_H_
#define NODE_H_

class Node {
private:
	int data_;
	Node* next_;
public:
	Node(int x);
	virtual ~Node();
	int data();
	Node* next();
	void next(Node* p);

};

#endif /* NODE_H_ */
